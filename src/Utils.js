import { Group } from "three";
import * as SkeletonUtils from 'three/examples/jsm/utils/SkeletonUtils.js';

export const clone = (object, x = 0, y = 0, z = 0) => {
    if (object && (object.isGroup || object.isMesh)) {
        const result = new Group();
        const clonedObject = SkeletonUtils.clone(object);
        result.add(clonedObject)
        clonedObject.clips = object.clips;
        result.position.set(x, y, z);
        return result

    } else {
        //notify("clone() works only with groups or mesh!", REASON_CANNOT);
        return null;
    }


}