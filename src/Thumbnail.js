import { AmbientLight, Color, LinearToneMapping, Scene, sRGBEncoding, Vector3 } from "three";
import { MeshStandardMaterial } from "three";
import { WebGLRenderer } from "three";
import { OrthographicCamera } from "three";


function parallelTraverse(a, b, callback) {
    callback(a, b);

    for (let i = 0; i < a.children.length; i++) {
        parallelTraverse(a.children[i], b.children[i], callback);
    }
}


function resetClonedSkinnedMeshes(source, clone) {
    const clonedMeshes = [];
    const meshSources = {};
    const boneClones = {};

    parallelTraverse(source, clone, function (sourceNode, clonedNode) {
        if (sourceNode.isSkinnedMesh) {
            meshSources[clonedNode.uuid] = sourceNode;
            clonedMeshes.push(clonedNode);
        }
        if (sourceNode.isBone) boneClones[sourceNode.uuid] = clonedNode;
    });

    for (let i = 0, l = clonedMeshes.length; i < l; i++) {
        const clone = clonedMeshes[i];
        const sourceMesh = meshSources[clone.uuid];
        const sourceBones = sourceMesh.skeleton.bones;

        clone.skeleton = sourceMesh.skeleton.clone();
        clone.bindMatrix.copy(sourceMesh.bindMatrix);

        clone.skeleton.bones = sourceBones.map(function (bone) {
            return boneClones[bone.uuid];
        });

        clone.bind(clone.skeleton, clone.bindMatrix);
    }
}


export class Thumbnail {
    constructor(width, height, zoom = 90) {
        this.camera = new OrthographicCamera(- width / 2, width / 2, height / 2, - height / 2, 0, 2000);
        this.renderer = new WebGLRenderer({
            preserveDrawingBuffer: true,
            antialias: true,
            alpha: true
        })

        this.renderer.toneMapping = LinearToneMapping;

        this.camera.zoom = zoom;
        this.camera.position.x = -30;
        this.camera.position.y = 50;
        this.camera.position.z = 30;
        this.camera.lookAt(0, 0, 0);
        this.renderer.setSize(width, height);
        this.scene = new Scene();
        this.scene.add(new AmbientLight("#fff", 1));
        //this.scene.background = new Color(0xeeeeee);
    }

    setZoom(zoom = 90) {
        this.camera.zoom = zoom;
    }

    generate(mesh, destination) {
        if (!mesh || !destination)
            return;




        const tempMesh = mesh.clone();
        resetClonedSkinnedMeshes(mesh, tempMesh)
        this.scene.add(tempMesh);
        tempMesh.position.set(0, 0, 0);
        //this.camera.position.x = 2;
        //this.camera.position.y = 2;
        //this.camera.position.z = 2;
        // this.camera.lookAt(0, 1, 0);
        this.camera.updateProjectionMatrix();
        this.renderer.render(this.scene, this.camera);

        const imgDestination = document.createElement('img');
        imgDestination.src = this.renderer.domElement.toDataURL("image/png");
        imgDestination.dataset.model = tempMesh.id;
        destination.appendChild(imgDestination);
        
        tempMesh.visible = false;
        return imgDestination.src;
    }

    getModelPlaceholder(domElement) {
        const id = parseInt(domElement.dataset.model);
        const modelClone = this.scene.getObjectById(id).clone();
        modelClone.visible = true;
        modelClone.userData.assetId = id;
        modelClone.traverse((o) => {
            if (o.isMesh) {

                o.material = new MeshStandardMaterial({
                    transparent: true,
                    opacity: 0.3,
                    color: "#ff00cc"
                })
            }
        })
        return modelClone;
    }

    getModelClone(id) {
        const modelClone = this.scene.getObjectById(id).clone();
        modelClone.visible = true;
        return modelClone;
    }
}