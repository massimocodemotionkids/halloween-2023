import * as THREE from 'three';

const ROTATION_AXIS = new THREE.Vector3(0, 1, 0);
const dummy = new THREE.Object3D();

export function createRandomInstances(object3D, randomPoints ) {

  var mesh;
  object3D.traverse((child) => {
    if (child.isMesh) {
      mesh = child;
    }
  });

  const geometry = mesh.geometry;
  const material = mesh.material.clone();

  const instancedMesh = new THREE.InstancedMesh(geometry, material, randomPoints.length);
  instancedMesh.name = mesh.name;

  for (let i = 0; i < randomPoints.length; i++) {

    dummy.position.set(randomPoints[i].x, randomPoints[i].y, randomPoints[i].z);
    const randomScale = 1 + Math.random() * 0.2
    dummy.scale.set(randomScale, randomScale, randomScale);
    dummy.rotation.y = randomPoints[i].rotation ? randomPoints[i].rotation : Math.random() * Math.PI * 2;
    dummy.rotation.x = Math.PI * (-0.1 * Math.random() * .2);
    dummy.rotation.z = Math.PI * (-0.1 * Math.random() * .2);
    dummy.updateMatrix();
    
    instancedMesh.setMatrixAt( i, dummy.matrix );

    
  }
  return instancedMesh
}