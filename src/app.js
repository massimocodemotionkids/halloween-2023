import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { SVGLoader } from 'three/examples/jsm/loaders/SVGLoader.js';
import { GPUPicker } from "./GPUPicker.js";
import { clone } from './Utils';
import { Thumbnail } from './Thumbnail';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js';
import { OutputPass } from 'three/examples/jsm/postprocessing/OutputPass.js';
import { createRandomInstances } from './RandomInstancedMeshGenerator.js'
import * as Stats from 'stats-js';
class HalloweenGame extends HTMLElement {
  constructor() {
    super();

    // Creare uno stile Shadow DOM
    const style = document.createElement('style');
    style.textContent = `
    :host {
      position: absolute;
      display: block;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      overflow: hidden;
      background: radial-gradient(circle, rgba(37, 0, 39, 0.9), rgba(0, 0, 0, 1));
    }
    `;
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(style);

    this.svgLoader = new SVGLoader();
    this.assetsUrl = this.getAttribute('url');
    this.svgUrl = this.querySelector('img').getAttribute('src');
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 0.01, 200);
    this.camera.position.x = 35;
    this.camera.position.z = 35;
    this.camera.position.y = 35;
    this.renderer = new THREE.WebGLRenderer({ antialias: false, alpha: false });
    this.renderer.toneMapping = THREE.LinearToneMapping;
    //this.renderer.toneMapping = THREE.ReinhardToneMapping;
    //this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.shadowMap.enabled = false;
    //this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    this.picker = new GPUPicker(this.renderer, this.scene, this.camera);
    this.thumbnail = new Thumbnail(256, 256, 30);
    this.shadowRoot.appendChild(this.renderer.domElement);
    this.stats = new Stats();
    this.stats.dom.style.left = 'unset';
    this.stats.dom.style.right = '10px';
    this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
    this.shadowRoot.appendChild(this.stats.dom);
    const renderScene = new RenderPass(this.scene, this.camera);
    renderScene.clearColor = new THREE.Color(0, 0, 0);
    renderScene.clearAlpha = 0;
    const params = {
      threshold: 0.1,
      strength: 0.45,
      radius: 0.2,
      exposure: 1
    };
    const bloomPass = new UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 1.5, 0.4, 0.85);
    bloomPass.threshold = params.threshold;
    bloomPass.strength = params.strength;
    bloomPass.radius = params.radius;

    const outputPass = new OutputPass();

    this.composer = new EffectComposer(this.renderer);
    this.composer.addPass(renderScene);
    
    if (!this.isMobile()) {
      this.composer.addPass(bloomPass);
    }
    //
    this.composer.addPass(outputPass);

    this.inFrameElementsBeforeRender = [];
    this.inFrameElements = [];
    this.assetDict = {};
    // Aggiungi la gestione del ridimensionamento
    window.addEventListener('resize', () => {
      this.onResize()
    });

    this.controls = new OrbitControls(this.camera, this.renderer.domElement);

    // Aggiungi luce ambientale
    this.ambientLight = new THREE.AmbientLight("#fff", 0.3);
    this.scene.add(this.ambientLight);
    window.ambientLight = this.ambientLight;
    this.cameraLight = new THREE.PointLight("#211", 50, 50, 0.1);
    //this.cameraLight.castShadow = true;
    //this.cameraLight.position.y = 1.68 / 2;

    window.cameraLight = this.cameraLight;

    this.scene.add(this.cameraLight);


    // Aggiungi 3 luci direzionali
    const directionalLight1 = new THREE.DirectionalLight(0xffffff, 0.5);
    //directionalLight1.castShadow = true;
    directionalLight1.position.set(2000, 2500, -2000);
    directionalLight1.shadow.mapSize.width = 1024 * 2;
    directionalLight1.shadow.mapSize.height = 1024 * 2;
    directionalLight1.shadow.camera.near = 0.1;
    directionalLight1.shadow.camera.far = 50;
    this.scene.add(directionalLight1);

    // const directionalLight2 = new THREE.DirectionalLight(0xffffff, 2);
    // directionalLight2.castShadow = true;
    // directionalLight2.position.set(0, 1, 0);
    // this.scene.add(directionalLight2);

    // const directionalLight3 = new THREE.DirectionalLight(0xffffff, 0.3);
    // directionalLight3.castShadow = true;
    // directionalLight3.position.set(0, 0, 1);
    // this.scene.add(directionalLight3);

    window.enterFullscreen = () => {
      const element = document.documentElement; // Elemento a cui applicare il fullscreen, generalmente l'intero documento
      if (element.requestFullscreen) {
        element.requestFullscreen();
      } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen(); // Firefox
      } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen(); // Chrome, Safari e Opera
      } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen(); // Internet Explorer
      }
    }

    window.hideFPS = () => {
      this.stats.dom.style.display = 'none';
    }
    window.isDebug = () => {
      const urlParams = new URLSearchParams(window.location.search);
      return urlParams.has('debug');
    }


    window.color = (colorString) => {
      return new THREE.Color(colorString)
    }
    window.firefly = (color, radius = 0.015) => {
      const fireflyGeometry = new THREE.IcosahedronGeometry(radius);
      const fireflyMaterial = new THREE.MeshStandardMaterial({ color: "black", emissive: color });
      return new THREE.Mesh(fireflyGeometry, fireflyMaterial);
    }
    //this.scene.fog = new THREE.Fog("#000", 6, 28);
    window.fog = (color, near = 6, far = 28) => {
      this.scene.fog = new THREE.Fog(color, near, far);
    };
    if (this.assetsUrl)
      this.loadAssets(this.assetsUrl, (assetsDictionary) => {
        // callback per quando tutti gli asset sono stati caricati
        //console.log('Loaded', assetsDictionary);

      }, (progress, assetName, assetMesh) => {
        // // callback per il progresso del caricamento
        assetMesh.traverse((o) => {
          if (o.isMesh) {
            // o.castShadow = true;
            // o.receiveShadow = true;
          }
        })
      });

    this.currentGroup = this.scene
    window.beginGroup = () => {
      const newGroup = new THREE.Group();
      this.currentGroup = newGroup;
      return newGroup

    }
    window.endGroup = () => {
      this.scene.add(this.currentGroup);
      this.currentGroup = this.scene;
    }
    window.Vector3 = THREE.Vector3;

    window.get = (assetName) => {

      const assetMesh = clone(typeof assetName == 'string' ? this.assetDict[assetName] : assetName, 0, 0, 0);
      if (!assetMesh) {
        console.warn("Cannot find", assetName)
        return
      }
      assetMesh.traverse((object) => {
        if (object.isMesh) {
          object.onBeforeRender = (renderer, scene, camera, geometry, material, group) => {
            this.inFrameElements.push(object)
          };
          if (assetName.indexOf('fluo') > -1) {
            object.material.emissive = color("#cddc39")
          }
        }
      })
      return assetMesh;
    }
    window.place = (assetName, x = 0, y = 0, z = 0, rotation = 0, rotationX = 0) => {
      const assetMesh = clone(typeof assetName == 'string' ? this.assetDict[assetName] : assetName, x, y, z);
      assetMesh.rotation.y = rotation * Math.PI * 2;
      assetMesh.rotation.x = rotationX * Math.PI * 2;
      this.currentGroup.add(assetMesh);
      return assetMesh;
    }

    window.getScreenshot = (asset, assetName) => {
      return this.getScreenshot(asset, assetName)
    };

    window.createSpline = (points) => {
      const spline = this.createSpline(points)
      return spline;
    }
    window.moveCamera = (spline) => {
      this.moveCamera(spline)
    };

    window.tube = (spline, closed = true) => {
      return this.tube(spline, closed);
    }

    this.render();

    window.createFloor = (width, height) => {
      return this.createPlane(width, height);
    }

    window.loadSvgAndCreateScene = (svgUrl, callback = (data) => {
      console.log(data)
    }) => {
      return this.loadSvgAndCreateScene(svgUrl, callback)
    }

    window.pick = (x, y) => {
      return this.pickObject(x, y);
    }
    window.restart = () => {
      this.restart()
    }
    window.speed = (speed) => {
      if (!this.camera.userData.cameraMovement)
        return

      this.camera.userData.cameraMovement.speed = speed;
    }
    /**
     * * Verifica se un oggetto è visibile nel frame della camera.
    * @param {THREE.PerspectiveCamera} camera - La camera prospettica.
    * @param {THREE.Object3D} object - L'oggetto 3D da verificare.
    * @returns {boolean} True se l'oggetto è nel frame, altrimenti false.
    */

    window.isInFrame = (object) => {
      // Creazione di un oggetto Frustum
      const frustum = new THREE.Frustum();
      this.camera.updateProjectionMatrix();
      this.camera.updateMatrixWorld();
      // Creazione di una matrice che rappresenta la proiezione della telecamera e la sua inversione nel mondo
      const matrix = new THREE.Matrix4().multiplyMatrices(this.camera.projectionMatrix, this.camera.matrixWorldInverse);

      // Aggiornamento del frustum con la matrice di proiezione e inversione
      frustum.setFromProjectionMatrix(matrix);

      // Verifica se il punto obj.position è fuori dalla vista del frustum
      return frustum.containsPoint(object.position);
    };

    window.randomPlace = (object3D, randomPoints) => {
      const mesh = createRandomInstances(object3D, randomPoints);
      this.scene.add(mesh);
      return mesh;
    }
    window.texture = (url) => {
      return new THREE.TextureLoader().load(url);
    }

  }

  isMobile = () => {
    return /iPhone|iPad|iPod|Android|webOS|BlackBerry|Windows Phone/i.test(navigator.userAgent)
  }

  getFXLevel = () => {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.has('fx');
  }

  pickObject = (x, y) => {
    const currentObjectUnderMouse = this.scene.getObjectById(this.picker.pick(x - this.renderer.domElement.getBoundingClientRect().left, y - this.renderer.domElement.getBoundingClientRect().top));
    if (currentObjectUnderMouse && currentObjectUnderMouse.isMesh) {
      this.dispatchEvent(new CustomEvent('hit', {
        detail: {
          subject: currentObjectUnderMouse
        }
      }));
      return currentObjectUnderMouse;
    }
  }

  loadSvgAndCreateScene(svgUrl, callback, finalCallback = (data) => {
    console.log("Loaded ", data)
  }) {
    const loader = this.svgLoader;

    loader.load(svgUrl, (data) => {
      const paths = data.paths;
      const images = data.xml.querySelectorAll('image');
      for (let image of images)
        console.log(image)
      //const group = new THREE.Group();

      paths.forEach((path) => {
        //console.log(path)
        const shapes = path.toShapes(true);

        shapes.forEach((shape) => {
          callback(shape, path)
        });
      });

      finalCallback(data);
    }, (progress) => { }, (error) => { console.log("ERROR", error) });
  }


  createPlane(width = 10, height = 10, segmentsWidth = 10, segmentsHeight = 10) {
    const geometry = new THREE.PlaneGeometry(width, height, segmentsWidth, segmentsHeight);
    const mesh = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: "#333" }))
    mesh.rotation.x = -Math.PI / 2;

    this.scene.add(mesh);
    return mesh
  }

  createSpline(points, closed = true) {
    const pointsVectors = [];
    for (let point of points) {
      pointsVectors.push(new THREE.Vector3(point.x, point.y, point.z));
    }

    const curve = new THREE.CatmullRomCurve3(pointsVectors, closed);

    return curve
  }

  tube(spline, closed = true, color = "#0f0") {
    const tubeGeometry = new THREE.TubeGeometry(spline, 600, 0.1, 24, closed);
    const tubeMaterial = new THREE.MeshStandardMaterial({ side: THREE.DoubleSide, color: color, roughness: 0 });
    const tubeMesh = new THREE.Mesh(tubeGeometry, tubeMaterial);
    return tubeMesh
  }

  restart() {
    if (this.camera.userData.cameraMovement) {
      this.camera.userData.cameraMovement.time = 0;
    }
  }

  stopCamera() {
    this.camera.userData.movementUpdate = null;
  }

  moveCamera(spline, initialSpeed = 0.00003, initialTime = 0) {
    this.camera.userData.cameraMovement = {
      curve: spline,
      time: initialTime, // Tempo sulla curva (0-1)
      speed: initialSpeed, // Velocità di spostamento
    };

    this.camera.userData.movementUpdate = (currentTime) => {
      // Muovi la telecamera lungo la curva
      this.controls.enablePan = false;
      this.controls.enableZoom = false;
      this.controls.enableDamping = false;
      this.controls.enableRotate = false;

      // Aggiorna il tempo sulla curva per spostare la telecamera
      if (!this.camera.userData.lastUpdate)
        this.camera.userData.lastUpdate = currentTime;
      const deltaTime = currentTime - this.camera.userData.lastUpdate;
      this.camera.userData.cameraMovement.time += deltaTime * this.camera.userData.cameraMovement.speed;

      this.camera.userData.lastUpdate = currentTime;

      // Resetta il tempo quando raggiungi la fine della curva (non dimenticare il resto)
      if (this.camera.userData.cameraMovement.time > 1) {
        this.camera.userData.cameraMovement.time = this.camera.userData.cameraMovement.time - 1;
        this.dispatchEvent(new CustomEvent('lap', {
          detail: {
            extratime: this.camera.userData.cameraMovement.time
          }
        }));
      }

      const pointOnCurve = this.camera.userData.cameraMovement.curve.getPointAt(this.camera.userData.cameraMovement.time);
      const nextPointOnCurve = this.camera.userData.cameraMovement.curve.getPointAt(((this.camera.userData.cameraMovement.time + 0.02) % 1));
      const lightPointOnCurve = this.camera.userData.cameraMovement.curve.getPointAt(((this.camera.userData.cameraMovement.time + 0.005) % 1));

      this.camera.position.copy(pointOnCurve);
      this.controls.target.copy(nextPointOnCurve);
      this.controls.update()
      this.cameraLight.position.copy(lightPointOnCurve);
      //this.cameraLight.position.y += 1;

    }
  }

  getScreenshot(asset, assetName = "Mesh") {
    // Creare un elemento div
    const assetDiv = document.createElement('div');

    assetDiv.title = assetName;
    assetDiv.style.position = 'relative';
    assetDiv.style.width = '256px'; // Imposta le dimensioni desiderate
    assetDiv.style.height = '256px';
    //Chiama la funzione Thumbnail.generate per inserire l'immagine nel div
    this.thumbnail.generate(asset, assetDiv);
    // Aggiungi l'elemento div al corpo del documento
    return assetDiv;


  }

  onResize() {
    // Ottieni le dimensioni dell'host
    const hostWidth = this.clientWidth;
    const hostHeight = this.clientHeight;
    // Aggiorna le dimensioni del canvas
    this.camera.aspect = hostWidth / hostHeight;
    this.camera.updateProjectionMatrix();


    this.renderer.setSize(hostWidth, hostHeight);
    this.composer.setSize(hostWidth, hostHeight);
  }

  render(currentTime) {

    this.stats.begin();
    if (this.camera.userData.movementUpdate) {
      this.camera.userData.movementUpdate(currentTime);
    }
    //this.renderer.render(this.scene, this.camera);
    this.composer.render();
    // }
    this.inFrameElementsBeforeRender = [...this.inFrameElements];
    this.inFrameElements = [];

    // Confronta gli oggetti inFrame prima e dopo il render
    const exitedFrameElements = this.inFrameElementsBeforeRender.filter(
      (element) => !this.inFrameElements.includes(element)
    );

    const enteredFrameElements = this.inFrameElements.filter(
      (element) => !this.inFrameElementsBeforeRender.includes(element)
    );

    const objectsChanged = exitedFrameElements.length > 0 || enteredFrameElements.length > 0;

    if (objectsChanged) {
      // Emetti l'evento on-screen-changes con le informazioni aggiuntive
      this.dispatchEvent(new CustomEvent('on-screen-changes', {
        detail: {
          inFrameElements: this.inFrameElements,
          exitedFrameElements: exitedFrameElements,
          enteredFrameElements: enteredFrameElements
        }
      }));
    }
    this.stats.end();
    this.dispatchEvent(new CustomEvent('on-render', {
      detail: {
        camera: this.camera,
        renderer: this.renderer,
        composer: this.composer
      }
    }));
    requestAnimationFrame((currentTime) => this.render(currentTime));
  }

  // Funzione di confronto array per verificare se gli elementi sono cambiati
  arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length) return false;
    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) return false;
    }
    return true;
  }



  loadAssets(textfile, loadedAllCallback, progressCallback) {
    fetch(textfile)
      .then(response => response.text())
      .then(data => {
        const lines = data.split('\n');
        const valid = lines.filter((line) => {
          return (line.indexOf('gltf') > 0);
        });

        const totalNumberOfAssets = valid.length;
        let loadedAssets = 0;
        this.dispatchEvent(new CustomEvent('loading-assets', {
          detail: {
            url: textfile
          }
        }));
        function isLast(assetName, assetMesh) {
          loadedAssets++;
          progressCallback(loadedAssets / totalNumberOfAssets, assetName, assetMesh);

          return loadedAssets === totalNumberOfAssets;
        }

        const alreadyUsedNames = {}

        valid.forEach((line) => {
          const parts = line.split('/');
          const filePath = line;
          var assetName = parts[parts.length - 1].trim().replace('.gltf', '');

          const loader = new GLTFLoader();
          loader.load(filePath, (gltf) => {
            if (!alreadyUsedNames[assetName]) {
              alreadyUsedNames[assetName] = true;
            } else {
              //console.warn(`${assetName} already in use, overwriten with ${filePath}`)
            }
            this.assetDict[assetName] = gltf.scene;
            this.dispatchEvent(new CustomEvent('asset-loaded', {
              detail: {
                name: assetName,
                asset: this.assetDict[assetName]
              }
            }));

            if (isLast(assetName, this.assetDict[assetName])) {
              loadedAllCallback(this.assetDict);
              this.dispatchEvent(new CustomEvent('assets-ready', { detail: this.assetDict }));
              if (this.svgUrl)
                this.loadSvgAndCreateScene(this.svgUrl, (shape, path) => {
                  this.dispatchEvent(new CustomEvent('level-node', {
                    detail: {
                      shape,
                      path
                    }
                  }));
                }, (data) => {
                  this.dispatchEvent(new CustomEvent('level-loaded', {
                    detail: data
                  }));
                });
            }
          });
        });
      })
      .catch((reason) => {
        console.warn(reason);
      });
  }
}



customElements.define('halloween-game', HalloweenGame);
